plugins {
    id("org.gradle.toolchains.foojay-resolver-convention") version "0.5.0"
}
rootProject.name = "boardgame-planner"
include("backend", "ui")

include("backend:domain")
include("backend:adapter-in")
include("backend:adapter-out")
include("backend:application")
include("application")
include("backend:application")
findProject(":backend:application")?.name = "application"
