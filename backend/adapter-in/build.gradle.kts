plugins {
    kotlin("jvm")
}

group = "be.kennymaes"
version = "unspecified"

repositories {
    mavenCentral()
}

dependencies {
    implementation(project(":backend:application"))
    implementation("org.springframework.boot:spring-boot-starter-web")
    testImplementation("org.jetbrains.kotlin:kotlin-test")
}

tasks.test {
    useJUnitPlatform()
}
kotlin {
    jvmToolchain(17)
}