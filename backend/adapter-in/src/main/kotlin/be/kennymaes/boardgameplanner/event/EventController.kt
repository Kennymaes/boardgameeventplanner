package be.kennymaes.boardgameplanner.event

import org.springframework.http.HttpStatus.CREATED
import org.springframework.http.HttpStatus.OK
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/events")
class EventController(var service: EventService) {

    @ResponseStatus(OK)
    @GetMapping()
    fun getEvents(): List<EventDto> = service.getEvents()

    @ResponseStatus(OK)
    @GetMapping("/{id}")
    fun getEventById(@PathVariable id: String): EventDto? = service.getEvent(id)

    @ResponseStatus(CREATED)
    @PostMapping("/test")
    fun create(@RequestBody eventDto: UpsertEventDto): EventDto = service.createEvent(eventDto)

}