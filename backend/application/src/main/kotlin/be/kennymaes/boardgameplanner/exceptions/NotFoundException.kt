package be.kennymaes.boardgameplanner.exceptions

class NotFoundException(message: String) : RuntimeException(message)
