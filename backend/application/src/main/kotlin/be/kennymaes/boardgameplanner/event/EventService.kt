package be.kennymaes.boardgameplanner.event

import be.kennymaes.boardgameplanner.exceptions.NotFoundException
import org.springframework.stereotype.Service
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter


@Service
class EventService(private val repository: EventRepository) {

    fun getEvents(): List<EventDto> = repository.findAll().map { it.toDto() }
    fun getEvent(id: String): EventDto {
        return repository.findById(id)?.toDto() ?: throw NotFoundException("No event found with id: $id")
    }

    fun createEvent(eventDto: UpsertEventDto): EventDto = repository.createEvent(eventDto.toDomain()).toDto()

}

fun Event.toDto(): EventDto = EventDto(
    id = id,
    name = name,
    description = description,
    open = open,
    status = status.name,
    date = date.toString()
)

fun UpsertEventDto.toDomain(): Event {
    val formatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME
    return Event(
        name = name,
        description = description,
        open = open,
        status = Status.valueOf(status),
        date = LocalDateTime.parse(date, formatter)
    )
}