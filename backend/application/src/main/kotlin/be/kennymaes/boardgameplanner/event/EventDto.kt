package be.kennymaes.boardgameplanner.event

import java.util.*

data class EventDto(
    val id: UUID,
    val name: String,
    val description: String,
    val open: Boolean,
    val status: String,
    val date: String?
)