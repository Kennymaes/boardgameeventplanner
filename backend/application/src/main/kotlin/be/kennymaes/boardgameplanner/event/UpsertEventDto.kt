package be.kennymaes.boardgameplanner.event

data class UpsertEventDto(
    val name: String,
    val description: String,
    val open: Boolean,
    val status: String,
    val date: String?
)