plugins {
    kotlin("jvm")
}

group = "be.kennymaes"
version = "unspecified"

repositories {
    mavenCentral()
}

dependencies {
    api(project(":backend:domain"))
    implementation("org.springframework.boot:spring-boot-starter-web")
    testImplementation("org.jetbrains.kotlin:kotlin-test")
}

tasks.test {
    useJUnitPlatform()
}
kotlin {
    jvmToolchain(17)
}