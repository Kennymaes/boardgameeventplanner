package be.kennymaes.boardgameplanner

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class BoardgamePlannerApplication

fun main(args: Array<String>) {
	runApplication<BoardgamePlannerApplication>(*args)
}
