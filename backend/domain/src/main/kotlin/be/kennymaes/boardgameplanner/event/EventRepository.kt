package be.kennymaes.boardgameplanner.event

interface EventRepository {

    fun findAll(): List<Event>
    fun findById(id: String): Event?
    fun createEvent(event: Event): Event
}