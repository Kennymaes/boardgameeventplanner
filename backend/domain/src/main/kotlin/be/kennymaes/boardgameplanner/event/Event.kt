package be.kennymaes.boardgameplanner.event

import java.time.LocalDateTime
import java.util.*

class Event(
    val id: UUID = UUID.randomUUID(),
    val name: String,
    val description: String,
    val open: Boolean = true,
    val status: Status,
    val date: LocalDateTime? = null
) {
    init {
        require(description.length <= 250) { "Description must be less then 250 characters" }
    }
}

enum class Status {
    PLANNED,
    CANCELED
}