package be.kennymaes.boardgameplanner.event

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertDoesNotThrow
import java.time.LocalDateTime
import kotlin.test.assertFailsWith

class EventTest {
    @Test
    fun `create Event with minimal input`() {
        assertDoesNotThrow { Event(name = "name", description = "Description", status = Status.PLANNED) }
    }

    @Test
    fun `invalid Event when creating with description more then 250 characters`() {
        val toLongDescription =
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque porta eget mauris sed eleifend. Aenean varius a dolor eu convallis. Vivamus felis ligula, volutpat in gravida at, finibus ac ante. Proin porta arcu ac suscipit pharetra. Pellentesque in t"

        val exception = assertFailsWith<IllegalArgumentException>(
            message = "No exception was found!",
            block = { Event(name = "name", description = toLongDescription, date = LocalDateTime.now(), status = Status.PLANNED) }
        )

        assert(exception.message.equals("Description must be less then 250 characters"))
    }
}

