package be.kennymaes.boardgameplanner.event

import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface EventJpaRepository: JpaRepository<EventEntity, UUID>