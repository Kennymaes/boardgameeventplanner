package be.kennymaes.boardgameplanner.event

import org.springframework.stereotype.Repository
import java.util.*
import kotlin.jvm.optionals.getOrNull

@Repository
class EventRepositoryImpl(private val eventJpaRepository: EventJpaRepository): EventRepository {
    override fun findAll(): List<Event> {
        return eventJpaRepository.findAll().map { it.toDomain() }
    }

    override fun findById(id: String): Event? {
        return eventJpaRepository.findById(UUID.fromString(id)).getOrNull()?.toDomain()
    }

    override fun createEvent(event: Event): Event {
        return eventJpaRepository.save(event.toEntity()).toDomain()
    }
}

fun EventEntity.toDomain(): Event =
    Event(
        id = this.id,
        name = this.name,
        description = this.description,
        open = this.open,
        status = this.status,
        date = this.date
    )

fun Event.toEntity(): EventEntity =
    EventEntity(id,name, description, open, status, date)
