package be.kennymaes.boardgameplanner.event

import jakarta.persistence.*
import java.time.LocalDateTime
import java.util.*

@Entity
@Table(name = "Event")
class EventEntity(
    @Id
    var id: UUID,
    var name: String,
    var description: String,
    var open: Boolean,
    @Enumerated(value = EnumType.STRING)
    var status: Status,
    var date: LocalDateTime? = null

)